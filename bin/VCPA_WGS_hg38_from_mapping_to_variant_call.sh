#!/bin/bash -x
#####
# Created: July-31-2017
# Start to process whole steps
#####

echo "Begin script"
export BIN="/mnt/adsp/vcpa-pipeline/bin"

###
# Getting SGE ENV
###
source /etc/profile.d/sge.sh

###
# Getting processing code from git repo
###
export VCPADIR="/mnt/adsp/vcpa-pipeline"

source $VCPADIR/VCPA/stage0/pipeline.ini
source $VCPADIR/VCPA/stage0/common_functions.sh
source $VCPADIR/VCPA/stage0/seqfile_functions.sh

###
# Setting Global parameters
# SM is Sample name
# HOST is Hostname
# PRJ_ID is Project ID
# FSIZE is Sample attribute
# RID is Project running number
###
export SM="GIAB-Sample"
export HOST=$HOSTNAME
export PRJ_ID="99"
export S3PATH=""
export FSIZE="21g"
export RID="1"

cd $VCPADIR
###
# Getting original BAM file from local
###
export RESULTSPATH="/mnt/adsp/bam/results"

SEQFILE="GIAB-Sample.bam"

###
# Checking original BAM/CRAM file
###
#    if [ "$(isCRAM $BAM_DIR/$SEQFILE)" == "1" ];then
#       echo "CRAM2BAM"
#       CRAM2BAM "$BAM_DIR/$SEQFILE" "$BAM_DIR/$SM.bam" $REF_FASTA
#       rm -v "$BAM_DIR/$SEQFILE"
#    else
#       mv -v "$BAM_DIR/$SEQFILE" "$BAM_DIR/$SM.bam"
#    fi

#    if [ -s $BAM_DIR/$SEQFILE ];then
#      mv -v $BAM_DIR/$SEQFILE $BAM_DIR/$SM.bam
#    fi

#    if [ "$(isSorted $BAM_DIR/$SM.bam)" == "0" ];then
#      ulimit -Hn 65536
#      ulimit -Sn 65536
#       echo "Sort-downloaded-file"
#       $SAMBAMBA sort --tmpdir=$TMP_DIR $BAM_DIR/$SM.bam && \
#          mv -v $BAM_DIR/$SM.sorted.bam $BAM_DIR/$SM.bam
#       mv -v $BAM_DIR/$SM.sorted.bam.bai $BAM_DIR/$SM.bam.bai
#    fi

#  ZCT=$(readgroup_cnt $BAM_DIR/$SM.bam)
#    if [ "$ZCT" -eq 0 ];then
#       echo "Adding-Generic-RG"
#       time add_generic_read_group $BAM_DIR/$SM.bam
#    fi

ID=$SM

###
# Setting the JSON file for sample
###
cp  $BIN/vcpa_pipeline_SAMPLE.json $BIN/vcpa_pipeline_$ID.json
sed -i "s/ID/$ID/g" $BIN/vcpa_pipeline_$ID.json

###
# processing stage0
# SAMBAMBAreadname_sort
# BAMToBWAmem
###
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_SAMBAMBAreadname_sort.wdl $BIN/vcpa_pipeline_$ID.json
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_BAMToBWAmem.wdl $BIN/vcpa_pipeline_$ID.json

###
# processing stage1
# SortBam
# MarkDupAllBam
# DepthOfCoverage
# MarkDepthOfCoverage
# MQ30
###
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_SortBam.wdl $BIN/vcpa_pipeline_$ID.json
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_MarkDupAllBam.wdl $BIN/vcpa_pipeline_$ID.json

java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_MQ30.wdl $BIN/vcpa_pipeline_$ID.json
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_DepthOfCoverage.wdl $BIN/vcpa_pipeline_$ID.json
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_MarkDepthOfCoverage.wdl $BIN/vcpa_pipeline_$ID.json

###
# processing stage2a
# BaseRecalibrator
# RealignTargetCreator
# IndelRealigner
# MergeindelRealigner
###
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_Bcal.wdl $BIN/vcpa_pipeline_$ID.json
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_RTC.wdl $BIN/vcpa_pipeline_$ID.json
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_IndelRealigner.wdl $BIN/vcpa_pipeline_$ID.json
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_MergeindelRealigner.wdl $BIN/vcpa_pipeline_$ID.json

###
# processing stage2b
# HaplotypeCaller
# VariantEval
###
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_HC_full_bam.wdl $BIN/vcpa_pipeline_$ID.json
java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_VariantEval.wdl $BIN/vcpa_pipeline_$ID.json
#java  -jar $BIN/cromwell-28_2.jar run $VCPADIR/WDL/vcpa_workflow_CollectInsertSizeMetrics.wdl $BIN/vcpa_pipeline_$ID.json


