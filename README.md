
## The VCPA is SNP/Indel Variant Calling Pipeline and data management tool used for analysis of whole genome and exome sequencing (WGS/WES) for the Alzheimer�s Disease Sequencing Project.

### [Overview of VCPA wiki](https://bitbucket.org/NIAGADS/vcpa-pipeline/wiki/Home)

 VCPA consists of two independent but linkable components: pipeline and database. 
 
 The pipeline implements are coded in Workflow Description Language (WDL) and are fully optimized 
 for the Amazon elastic compute cloud environment. This includes steps for processing raw sequence 
 reads including read alignment and variant calling using GATK. 
 
 The tracking database allows users to dynamically view the statuses of jobs running and the quality 
 metrics reported by the pipeline. 
 
 Users can thus monitor the production process and diagnose if any problem arises during the procedure. 
 All quality metrics (>100 collected per processed genome) are stored in the database, thus facilitating 
 users to compare, share and visualize the results.
 







