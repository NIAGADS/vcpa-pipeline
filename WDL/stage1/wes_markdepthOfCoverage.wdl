#####
# Created: May-01-2017
# Stage1  markdepthOfCoverage for API
#####


task wes_markdepthOfCoverage {

###
# Input requirements
# File is from input_bam
# Library is on VCPA_LIB
###

  String RG_results
  String tmp
  String log
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")
  String VCPA_LIB

  command {

        FILENAME=`basename ${file_basename}.bam .bam`
        qsub \
            -N depth-$FILENAME-mark   \
            -cwd  \
            -o ${log}/depthOfCoverage-mark.log \
            -V \
            -hold_jid depth-$FILENAME-wes \
            -j y ${VCPA_LIB}/stage1/wes_markCompletedDepthOfCoverage.sh -i ${RG_results}/bam/$FILENAME.sorted.dupmarked.bam -p "${RG_results}/bam/$FILENAME"  -b "${VCPA_LIB}/stage0"

  }
  output {
  String response = read_string(stdout())
  }
}
