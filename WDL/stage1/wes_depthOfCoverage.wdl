#####
# Created: May-01-2017
# Stage1  Check data-quality of mapping (pre-VCF check)
#         Sambamba depth
#####


task wes_depthOfCoverage {

###
# Input requirements
# Use tools are included SAMBAMBA, SAMTOOLS
# File is from input_bam
# Library is on VCPA_LIB
###

  File GATK
  String RG_results
  String tmp
  String log
  File input_bam
  String file_basename = sub(input_bam, "\\.bam", "")
  String VCPA_LIB
  String TARGET

  command {

  #  count depthOfCoverage between 5x, 10x, 20x, 30x, 40x, 50x coverage

       if [ ! -s "${RG_results}/bam/$FILENAME.depth.txt" ];then

        FILENAME=`basename ${file_basename}.bam .bam`
        qsub \
            -N depth-$FILENAME   \
            -cwd  \
            -o ${log}/depthOfCoverage-wes.log \
            -V \
            -hold_jid markDup* \
            -j y ${VCPA_LIB}/stage1/wes_depthOfCoverage.sh -i ${RG_results}/bam/$FILENAME.sorted.dupmarked.bam  -p "${RG_results}/bam/$FILENAME"  -b "${VCPA_LIB}/stage0" -l ${TARGET}
       fi
  }
  output {
  String response = read_string(stdout())
  }
}
