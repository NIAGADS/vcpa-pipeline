#####
# Created: April-19-2018
# Stage2b 
#         PICARD CollectInsertSizeMetrics
#####

task collectInsertSize {


###
# Input requirements
# Use tools are included PICARD
# File is from input_bam
# Library is on VCPA_LIB
###

  File PICARD
  File input_bam
  String tmp
  String log
  String RG_results
  String VCPA_LIB
  String file_basename = sub(input_bam, "\\.bam", "")


  command {

  FILENAME=`basename ${file_basename}.bam .bam`
  qsub \
       -N CInsSz-$FILENAME \
       -cwd  \
       -o ${log}/collectInsertSize.log \
       -l h_vmem=8.2G  \
       -V \
       -hold_jid IndelRealign-merge-$FILENAME \
       -j y ${VCPA_LIB}/stage2b/collectInsertSize.sh -i "${RG_results}/bam/$FILENAME.hg38.realign.bqsr.bam" -b "${VCPA_LIB}/stage0" -p "${RG_results}/vcf/$FILENAME" -m "${RG_results}" -l ${log}


  }

  output {
  #File eval_vcf = "${file_basename}.eval.txt"
   String response = read_string(stdout())
  }
}