#####
# Created: April-19-2018
# Stage1 Count reads with mapping quality >=30
#         Sambamba
#####

import "WDL/stage1/mq30.wdl" as mq30

workflow Tutorial_vcpa_steps {

###
# Input requirements
# Use tools are included SAMBAMBA
# File is from input_bam
# Library is on VCPA_LIB
###


  File SAMBAMBA
  File input_bam
  String tmp
  String log
  String RG_results
  String VCPA_LIB


  call mq30.mq30  {

         input:
         SAMBAMBA = SAMBAMBA,
         input_bam = input_bam,
         tmp = tmp,
         log = log,
         RG_results = RG_results,
         VCPA_LIB = VCPA_LIB
  }


  meta {
    version: "1.0.0"
    SAMBAMBA: "0.6.5"
  }
}
