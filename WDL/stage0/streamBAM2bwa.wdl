#####
# Created: July-31-2017
# Stage0 Roll back and Map reads to reference genome
#         Picard RevertSam
#         Picard MarkIlluminaAdapters
#####


task StreamBam2bwa {

###
# Input requirements
# Use tools are included BWA, PICARD, SAMTOOLS
# File is from input_bam
# Library is on VCPA_LIB, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###
  File PICARD
  File SAMTOOLS
  File BWA
  String THREADS
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File input_bam
  String tmp
  String log
  String RG_results
  String file_basename = sub(input_bam, "\\.bam", "")
  String VCPA_LIB

  command {

  # uses RevertSam to convert input BAM to SAM in stdout
  # @param SEQFILE - input BAM, single read-group, sorted by name, using original qualities
  # Output - $RG.aligned.sam is a readgroup sam file (note: per read group)
   RGRPS=`bash ${VCPA_LIB}/readgroup.sh -i ${file_basename}.bam`
   THREADS="2" #THREADS=`bash ${VCPA_LIB}/bwathreads.sh -i ${file_basename}.bam`
   #MEM=`bash ${VCPA_LIB}/bwamem.sh -i ${file_basename}.bam`
   echo $RGRPS | sed 's/ /\n/g' > ${file_basename}.rg.log
   wc -l ${file_basename}.rg.log | cut -b 1-2 -  > ${file_basename}.num.log
   NUM=`cat ${file_basename}.num.log`
   echo $THREADS >> ${file_basename}.bam2bwa.log
   echo $MEM >> ${file_basename}.bam2bwa.log

   if [ $NUM == "0" ];then
           RG = "A"  ## if there is no readgroup will name A
           echo $RG >> ${file_basename}.bam2bwa.log
           qsub \
               -N BAM2bwa-$RG \
               -cwd  \
               -o ${log}/bam2bwa.log  \
               -pe DJ $THREADS \
               -V \
               -hold_jid bamba-sort-flt-$RG  \
               -j y ${VCPA_LIB}/stage0/streamBAM2bwa.sh -i ${RG_results}/bam/A.sorted-byname.bam -r $RG -t $THREADS -p "${RG_results}/bam" -b "${VCPA_LIB}/stage0" -m "${RG_results}"

   elif [ $NUM == "1" ];then
          RG=$RGRPS
          echo $RG >> ${file_basename}.bam2bwa.log
          qsub \
               -N BAM2bwa-$RG \
               -cwd  \
               -o ${log}/bam2bwa.log  \
               -pe DJ $THREADS \
               -V \
               -hold_jid bamba-sort-flt-$RG  \
               -j y ${VCPA_LIB}/stage0/streamBAM2bwa.sh -i ${RG_results}/bam/1.sorted-byname.bam -r $RG -t $THREADS -p "${RG_results}/bam" -b "${VCPA_LIB}/stage0" -m "${RG_results}"
              
           
   else

     for RG in $RGRPS;do

       echo "RG IS " $RG >> ${file_basename}.bam2bwa.log
       qsub \
            -N BAM2bwa-$RG \
            -cwd  \
            -o ${log}/bam2bwa.log  \
            -pe DJ $THREADS \
            -V \
            -hold_jid bamba-sort-flt-$RG  \
            -j y ${VCPA_LIB}/stage0/streamBAM2bwa.sh -i ${RG_results}/bam/$RG.sorted-byname.bam -r $RG -t $THREADS -p "${RG_results}/bam" -b "${VCPA_LIB}/stage0" -m "${RG_results}"

     done
   fi
  }

  output {
  #File aligned_sam =  "$RG.aligned.mark.sam"
  String response = read_string(stdout())
  }
}
