#####
# Created: May-01-2017
# Stage2a Recalibrate reads on BAMs
#         GATK RealignTargetCreator
#####


task rtc {


###
# Input requirements
# Use tools are included GATK
# File is from input_bam
# Library is on VCPA_LIB, KNOWN, DBSNP, GOLD, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###

  File GATK
  String tmp
  String log
  String THREADS
  String RG_results
  String VCPA_LIB
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  File KNOWN
  File KNOWN_index
  File DBSNP
  File DBSNP_index
  File GOLD
  File GOLD_index
  String file_basename = sub(input_bam, "\\.bam", "")


  command {

    FILENAME=`basename ${file_basename}.bam .bam`
    #MEM=`bash ${VCPA_LIB}/maxmem.sh -i ${file_basename}.bam`
    #THREADS=`bash ${VCPA_LIB}/bwathreads.sh -i ${file_basename}.bam`

    if [ ! -s "${RG_results}/bam/$FILENAME.indels.intervals" ];then
     qsub \
            -N RTC-$FILENAME  \
            -cwd  \
            -o ${log}/rtc.log \
            -V \
            -hold_jid BCal-$FILENAME \
            -j y ${VCPA_LIB}/stage2a/rtc.sh -i "${RG_results}/bam/$FILENAME.sorted.dupmarked.bam" -b "${VCPA_LIB}/stage0" -p "${RG_results}/bam/$FILENAME" -t ${THREADS} -m "${RG_results}"



    fi
  }

  output {
  #File rtc_intervals  = "${file_basename}.indels.intervals"
   String response = read_string(stdout())
  }
}
