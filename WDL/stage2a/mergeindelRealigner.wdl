#####
# Created: May-01-2017
# Stage2a Recalibrate reads on BAMs
#         Sambamba merge
#         Sambamba index
#####


task mergeindelRealigner {


###
# Input requirements
# Use tools are included SAMBAMBA, SAMTOOLS, SAMBLASTER
# File is from input_bam
# Library is on VCPA_LIB, ref_fasta, ref_dict, ref_alt, ref_amb, ref_ann, ref_bwt, ref_pac, ref_sa, ref_fai
###

  File SAMBAMBA
  File SAMTOOLS
  File SAMBLASTER
  String tmp
  String log
  String THREADS
  String RG_results
  String VCPA_LIB
  File input_bam
  File ref_fasta
  File ref_dict
  File ref_alt
  File ref_amb
  File ref_ann
  File ref_bwt
  File ref_pac
  File ref_sa
  File ref_fai
  String file_basename = sub(input_bam, "\\.bam", "")


  command {


    chrlists=`seq -f "chr%g" 1 22`
    for CHR in $chrlists chrX chrY chrM other unmapped;do
      COMPRESSION=9
      FILENAME=`basename ${file_basename}.bam .bam`
      #MEM=`bash ${VCPA_LIB}/maxmem.sh -i ${file_basename}.bam`
      #THREADS=`bash ${VCPA_LIB}/bwathreads.sh -i ${file_basename}.bam`
      MSTR+="${RG_results}/bam/$FILENAME.indelrealign.$CHR.bam "
    done
      if [ ! -s "${RG_results}/bam/$FILENAME.hg38.realign.bqsr.bam" ];then

         qsub \
            -N IndelRealign-merge-$FILENAME \
            -cwd  \
            -o ${log}/indelrealignmerge.log \
            -V \
            -hold_jid IndelRealign-$FILENAME-* \
            -j y ${VCPA_LIB}/stage2a/indelrealignmerge.sh -i "$MSTR" -b "${VCPA_LIB}/stage0" -p "${RG_results}/bam/$FILENAME" -t ${THREADS} -m "${log}" -s "$MSTR" -c $COMPRESSION


      fi

  }

  output {
  #File indelRealigner_bam  = "${file_basename}.hg38.realign.bqsr.bam"
   String response = read_string(stdout())
  }
}
