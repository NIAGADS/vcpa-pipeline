#!/bin/bash

export DIR="/mnt/adsp"
source $DIR/VCPA/stage0/pipeline.ini
source $DIR/VCPA/stage0/common_functions.sh
source $DIR/VCPA/stage0/seqfile_functions.sh

while getopts di:t:m: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      i) SAMPLE="$OPTARG";;
      t) THREADS="$OPTARG";;
      m) RMEM="$OPTARG";;
   esac
done

 MEM=$(adjustWorkingMem $RMEM $THREADS)
 echo "${MEM}"
