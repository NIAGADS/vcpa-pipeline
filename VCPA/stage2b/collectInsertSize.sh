#!/bin/bash
#####
# Created: April-19-2018
# Stage2b 
#         PICARD CollectInsertSizeMetrics
#####
# collectInsertSize.sh
#
# @param (-i INPUTBAM)     - input BAM
# @param (-p PREFIX)       - out-file naming prefix
# @param (-b LIB)          - lib
# @param (-l Logs)         - logs
# @param (-m TMP)          - tmp



while getopts dn:i:p:b:l:m: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      b) LIB="$OPTARG";;
      l) L="$OPTARG";;
      m) TMP="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)


track_item current_operation "CollectInsertSizeMetrics"

     # set default PCRMODEL to CONSERVATIVE, overriden by getopts     
     java -Xmx4G -jar $PICARD CollectInsertSizeMetrics \
          TMP_DIR=${TMP} \
          INPUT=${INPUTBAM} \
          OUTPUT=${PREFIX}-insert_size_metric.tsv \
          HISTOGRAM_FILE=${PREFIX}.histogram.pdf \
          MINIMUM_PCT=0.5
     # MEAN_INSERT_SIZE
     MSE=$(head -8 ${PREFIX}-insert_size_metric.tsv | tail -1 | cut -f5)

     track_item mean_insert_sz $MSE
     


if [ -z $RESULTSPATH ];then
     export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
fi

#if [ ! -z $RESULTSPATH ];then
# echo "uploading tsv"
# aws s3 cp --only-show-errors "${PREFIX}-insert_size_metric.tsv" "$RESULTSPATH/"

#fi

