#!/bin/bash -x

source /mnt/adsp/users/user/adsp-wes-pipeline/pipeline.ini
source /mnt/adsp/users/user/adsp-wes-pipeline/lib/common_functions.sh

BASE=/mnt/data3/pVCF-production/20k-WES
Axiom=/mnt/adsp/Axiom_Exome_Plus.genotypes.all_populations.poly.hg38.vcf.gz

while getopts dnt:i:p:c: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      c) CHR="$OPTARG";;
      n) PCRMODEL="NONE";;   # override PCRMODEL to NONE
   esac
done

VAR=""
for CHR in {1..22} X Y M;do
    VAR+="--variant $BASE/CHR$CHR.merged.g.vcf.gz "
done

/mnt/adsp/users/user/gatk-4.1.1.0/gatk --java-options "-Xmx90g -Xms90g" \
             VariantRecalibrator \
             --resource:dbsnp,known=true,training=false,truth=false,prior=2 $DBSNP \
             --resource:mills,known=false,training=true,truth=true,prior=12 $GOLD \
             --resource:axiomPoly,known=false,training=true,truth=false,prior=10 $Axiom \
             -an QD \
             -an FS \
             -an SOR \
             -an ReadPosRankSum \
             -an MQRankSum \
             -mode INDEL \
             -an InbreedingCoeff \
             -L /home/user/20k-WES/capture_regions.bed \
             --max-gaussians 4 \
             --trust-all-polymorphic  \
             -tranche 100.0 \
             -tranche 99.95 \
             -tranche 99.9 \
             -tranche 99.5 \
             -tranche 99.0 \
             -tranche 97.0 \
             -tranche 96.0 \
             -tranche 95.0 \
             -tranche 94.0 \
             -tranche 93.5 \
             -tranche 93.0 \
             -tranche 92.0 \
             -tranche 91.0 \
             -tranche 90.0 \
             $VAR \
             -O $BASE/gatk411.20k-WES.hg38.aws-batch_INDEL.recal.vcf.gz \
             --tranches-file $BASE/gatk411.20k-WES.hg38.aws-batch_INDEL.trances
