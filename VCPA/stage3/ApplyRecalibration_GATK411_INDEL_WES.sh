#!/bin/bash
#INDEL

set -x

source /mnt/adsp/users/user/adsp-wes-pipeline/pipeline.ini
source /mnt/adsp/users/user/adsp-wes-pipeline/lib/common_functions.sh


while getopts dnt:i:p:c: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      c) CHR="$OPTARG";;
      n) PCRMODEL="NONE";;   # override PCRMODEL to NONE
   esac
done

BASE=/mnt/data3/pVCF-production/20k-WES

RECAL=/mnt/data3/pVCF-production/20k-WES

/mnt/adsp/users/user/gatk-4.1.1.0/gatk --java-options "-Xmx10g -Xms10g" \
         ApplyVQSR \
         -O $BASE/tmp.chr$CHR.indel.recalibrated.vcf.gz \
         -V $BASE/CHR$CHR.merged.g.vcf.gz \
         -L /home/user/20k-WES/capture_regions.bed \
         --recal-file $RECAL/gatk411.20k-WES.hg38.aws-batch_INDEL.recal.vcf.gz \
         --tranches-file $RECAL/gatk411.20k-WES.hg38.aws-batch_INDEL.trances \
         --truth-sensitivity-filter-level 99.5 \
         --create-output-variant-index true \
         -mode INDEL
