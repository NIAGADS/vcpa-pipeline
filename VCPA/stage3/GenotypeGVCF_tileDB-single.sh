#!/bin/bash -x
#This code is for extention combined GVCF to do the genotype GVCF

source /mnt/adsp/pipeline.ini
source /mnt/adsp/lib/common_functions.sh

while getopts dnt:i:c:o:v: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      c) CHR="$OPTARG";;
      o) OUTFILE="$OPTARG";;
      n) PCRMODEL="NONE";;   # override PCRMODEL to NONE
      v) VAR="--variant $OPTARG";;
   esac
done


java -Djava.io.tmpdir=/mnt/adsp/tmp/ \
     -Xmx4g \
      -jar $GATK37 \
             -T GenotypeGVCFs \
             -R $REF_FASTA \
             --dbsnp $DBSNP \
             -nt $THREADS \
             $VAR \
             -L $CHR \
             -o "$OUTFILE"


