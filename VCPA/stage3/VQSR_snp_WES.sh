#!/bin/bash -x

source /mnt/adsp/users/user/adsp-wes-pipeline/pipeline.ini
source /mnt/adsp/users/user/adsp-wes-pipeline/lib/common_functions.sh


BASE=/mnt/data3/pVCF-production/20k-WES
HIGH=/mnt/adsp/1000G_phase1.snps.high_confidence.hg38.vcf.gz

while getopts dnt:i:p:c: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      c) CHR="$OPTARG";;
      n) PCRMODEL="NONE";;   # override PCRMODEL to NONE
   esac
done

VAR=""
for CHR in {1..22} X Y M;do
    VAR+="--variant $BASE/CHR$CHR.merged.g.vcf.gz "
done

/mnt/adsp/users/user/gatk-4.1.1.0/gatk --java-options "-Xmx90g -Xms90g" \
             VariantRecalibrator \
             --resource:hapmap,known=false,training=true,truth=true,prior=15 $HAPMAP \
             --resource:omni,known=false,training=true,truth=true,prior=12 $OMNI \
             --resource:1000G,known=false,training=true,truth=false,prior=10 $HIGH \
             --resource:dbsnp,known=true,training=false,truth=false,prior=2 $DBSNP \
             -an QD \
             -an FS \
             -an SOR \
             -an MQ \
             -an ReadPosRankSum \
             -an MQRankSum \
             -an InbreedingCoeff \
             -mode SNP \
             -L /home/user/20k-WES/capture_regions.bed \
             --trust-all-polymorphic \
             -tranche 100.0 \
             -tranche 99.95 \
             -tranche 99.9 \
             -tranche 99.8 \
             -tranche 99.6 \
             -tranche 99.5 \
             -tranche 99.4 \
             -tranche 99.3 \
             -tranche 99.0 \
             -tranche 98.0 \
             -tranche 97.0 \
             -tranche 90.0 \
             $VAR \
             -R $REF_FASTA \
             -O $BASE/gatk411.20k-WES.hg38.aws-batch_SNP.recal.vcf.gz \
             --tranches-file $BASE/gatk411.20k-WES.hg38.aws-batch_SNP.trances


