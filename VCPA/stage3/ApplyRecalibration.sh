#!/bin/bash -x

source /mnt/adsp/pipeline.ini
source /mnt/adsp/lib/common_functions.sh

while getopts dnt:i:p:c: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      c) CHR="$OPTARG";;
      n) PCRMODEL="NONE";;   # override PCRMODEL to NONE
   esac
done

NAME="***" # replace to your study name
VAR=""
[[ ! -z CHR ]] && L_ARG="-L $CHR"

#for C in {1..22} X Y M;do
C=${CHR/chr/}
   for i in {0..20000};do
      if [ -e GSTEP-GATK37/$NAME.hg38.tileDB.c$C-$i.genotypedGVCF.g.vcf.gz ];then
         VAR+="--input GSTEP-GATK37/$NAME.hg38.tileDB.c$C-$i.genotypedGVCF.g.vcf.gz "
      else
         break
      fi
   done
#done


#VAR=""
#for i in GSTEP-GATK37/$NAME.hg38.tileDB.c$C-*.gz;do
#      VAR+="--input $i "
#done



java -Xmx20g -Djava.io.tmpdir=$TMP_DIR -jar $GATK \
             -T ApplyRecalibration \
             -R $REF_FASTA \
             $VAR \
             $L_ARG \
             -mode SNP \
             -nt $THREADS \
             --ts_filter_level 99.0  \
             -recalFile new_results/$NAME.hg38.tileDB.recalibrate_SNP.recal \
             -tranchesFile new_results/$NAME.hg38.tileDB.recalibrate_SNP.tranches \
             -o new_results/$NAME.hg38.tileDB.recalibrate_SNP.$CHR.g.vcf.bgz
  # bcftools view -m2 -M2 \
  #        -v snps $NAME.hg38.tileDB.recalibrate_SNP.$CHR.g.vcf.bgz -Oz > $NAME.hg38.tileDB.recalibrate_SNP.$CHR.biallelic.g.vcf.bgz  && \
   #tabix -f $NAME.hg38.tileDB.recalibrate_SNP.$CHR.biallelic.g.vcf.bgz

