#!/bin/bash

source /mnt/adsp/pipeline.ini
source /mnt/adsp/lib/common_functions.sh

while getopts dh: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      h) HJID="-hold_jid $OPTARG";;    # job id/name for hold_jid
   esac
done


QSUB=qsub
THREADS=6

for CHR in {2..22} X Y M;do
#for CHR in 1;do
 if [ ! -s "*.hg38.tileDB.recalibrate_SNP.chr$CHR.g.vcf.bgz.tbi" ];then

    MEM=$(adjustWorkingMem 24G $THREADS)
    $QSUB -pe DJ $THREADS \
            -l h_vmem=$MEM \
            -hold_jid '*-VQSR.tileDB.all' \
            -N "*-AR.chr$CHR" \
            -o Logs/*-AR.chr$CHR.log \
            -V \
            -cwd \
            -j y \
            ApplyRecalibration.sh -t $THREADS -c chr$CHR
 fi
done
