#!/bin/bash
#####
# Created: May-01-2017
# Stage1 Mark duplicated reads
#         Samtools sort
#         Samblaster addmatetags (CCDG version)
#         Samtools sort
#####

# markDup-start.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib

while getopts dn:i:p:b: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      b) LIB="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)
SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item current_operation "MarkDup"


       if [ ! -s "${PREFIX}.sorted.dupmarked.bam" ];then
          $BAMUTIL dedup_lowmem  --in ${INPUTBAM} --out ${PREFIX}.sorted.dupmarked.bam --force --excludeFlags 0xB00

       fi

       ##This needs to check the bamUtil-NonPrimaryDedup output and then use one to flagstat
       #if [ ! -s "${PREFIX}.sorted.dupmarked.bam.bai" ];then
         track_item current_operation "Markdup-indexing"
         $SAMBAMBA index -t 4 ${PREFIX}.sorted.dupmarked.bam

       #fi

END=$(date +%s)
TM=$[$END-$SRT];
NOW=$(date '+%F+%T')
echo $TM
echo $(qstat -j $JOB_ID|grep maxvmem)

track_item markdup_merge_done_tm $NOW
track_item markdup_merge_duration $TM

  #  Quality check on flagstat output:
  #  total number of reads
  #  duplicated reads %
  #  mapped reads %
  #  paired reads %
  #  singleton %

       #if [ ! -s "${PREFIX}.sorted.dupmarked.bam.flagstat" ];then
         if [ -z $RESULTSPATH ];then
            export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
         fi

         track_item current_operation "MarkDup-flagstat"
         $SAMBAMBA  flagstat ${PREFIX}.sorted.dupmarked.bam > ${PREFIX}.sorted.dupmarked.bam.flagstat
          #&& track_item current_operation "MarkDup-Merge-Upload" \
         # && aws s3 cp ${PREFIX}.sorted.dupmarked.bam.flagstat $RESULTSPATH/${FPREFIX}.flagstat

       #fi

       rm -rf ${INPUTBAM}

       if [ ! -s "${PREFIX}.sorted.dupmarked.bam" ];then
          error-to-hannah "MarkDup-ErrorState on $IID;$SM"
          track_item current_operation "MarkDup-ErrorState"

       fi

     # Extract flagstat stats
     TOT=$(grep "in total"     ${PREFIX}.sorted.dupmarked.bam.flagstat|grep -Po "^\d+")
     SECD=$(grep "secondary" ${PREFIX}.sorted.dupmarked.bam.flagstat|grep -Po "^\d+")
     SUPP=$(grep "supplementary" ${PREFIX}.sorted.dupmarked.bam.flagstat|grep -Po "^\d+")
     DUPS=$(grep "duplicates"  ${PREFIX}.sorted.dupmarked.bam.flagstat|grep -Po "^\d+")
     MAPPED=$(grep "mapped ("  ${PREFIX}.sorted.dupmarked.bam.flagstat|grep -Po "^\d+")
     SINGLE=$(grep "singletons" ${PREFIX}.sorted.dupmarked.bam.flagstat|grep -Po "^\d+")
     PAIRED=$(grep "properly paired" ${PREFIX}.sorted.dupmarked.bam.flagstat|grep -Po "^\d+")

     DPCENT=$(echo $DUPS   | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     MPCENT=$(echo $MAPPED | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     PPCENT=$(echo $PAIRED | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     SPCENT=$(echo $SINGLE | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')

     # Upload flagstat stats
     track_item markdup_merge_reads $TOT
     track_item markdup_merge_dups $DUPS
     track_item markdup_merge_dups_pcent $DPCENT
     track_item markdup_merge_mapped_reads $MAPPED
     track_item markdup_merge_mapped_pcent $MPCENT
     track_item markdup_merge_paired_reads $PAIRED
     track_item markdup_merge_paired_pcent $PPCENT
     track_item markdup_merge_singletons   $SINGLE
     track_item markdup_merge_singletons_pcent $SPCENT

     track_item markdup_merge_supp_cnt $SUPP
     track_item markdup_merge_2ndary_cnt $SECD

     track_item markdup_merge_supp_dup_cnt $($SAMBAMBA view -c -F "supplementary and duplicate" ${PREFIX}.sorted.dupmarked.bam)
     track_item markdup_merge_unmapped_dup_cnt $($SAMBAMBA view -c -F "unmapped and duplicate" ${PREFIX}.sorted.dupmarked.bam)


     track_item markdup_merge_rg_cnt $(readgroup_cnt ${PREFIX}.sorted.dupmarked.bam)
     track_item markdup_merge_bam_size $(stat -c '%s' ${PREFIX}.sorted.dupmarked.bam)
