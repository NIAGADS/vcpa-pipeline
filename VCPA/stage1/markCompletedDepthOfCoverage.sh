#!/bin/bash
#####
# Created: May-01-2017
# Stage1  markdepthOfCoverage for API
#####

# markCompletedDepthOfCoverage.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-p PREFIX)    - out-file naming prefix

while getopts dn:i:p:b: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      b) LIB="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
source ${LIB}/seqfile_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)


INFILE=${PREFIX}.depth.txt
RSIZE=$(get_read_size ${INPUTBAM})
ATGC_refSize=2745186602 #hg38 - w/o N's
track_item current_operation "DepthOfCoverage-stats-upload"
#chrom chromStart      chromEnd        readCount       meanCoverage     5x	10x	20x	30x	40x	50x
# $1   $2              $3              $4              $5              $6       $7      $8      $9      $10     $11
#P5=$(grep "^chr" $INFILE | awk  '{SUM += $6} END{ printf "%0.2f", SUM/NR }')
P5=$(grep "^chr" $INFILE | awk -v GSIZE=$ATGC_refSize '{SUM += $6 * $3} END{ printf "%0.2f", SUM/GSIZE }')
track_item depth_5x $P5

#P10=$(grep "^chr" $INFILE | awk '{SUM += $7} END{ printf "%0.2f", SUM/NR }')
P10=$(grep "^chr" $INFILE | awk -v GSIZE=$ATGC_refSize  '{SUM += $7 * $3} END{ printf "%0.2f", SUM/GSIZE }')
track_item depth_10x $P10

#P20=$(grep "^chr" $INFILE | awk '{SUM += $8} END{ printf "%0.2f", SUM/NR }')
P20=$(grep "^chr" $INFILE | awk -v GSIZE=$ATGC_refSize '{SUM += $8 * $3} END{ printf "%0.2f", SUM/GSIZE }')
track_item depth_20x $P20

#P30=$(grep "^chr" $INFILE | awk '{SUM += $9} END{ printf "%0.2f", SUM/NR }')
P30=$(grep "^chr" $INFILE | awk -v GSIZE=$ATGC_refSize '{SUM += $9 * $3} END{ printf "%0.2f", SUM/GSIZE }')
track_item depth_30x $P30

#P40=$(grep "^chr" $INFILE | awk '{SUM += $10} END{ printf "%0.2f", SUM/NR }')
P40=$(grep "^chr" $INFILE | awk -v GSIZE=$ATGC_refSize '{SUM += $10 * $3} END{ printf "%0.2f", SUM/GSIZE }')
track_item depth_40x $P40

#P50=$(grep "^chr" $INFILE | awk '{SUM += $11} END{ printf "%0.2f", SUM/NR }')
P50=$(grep "^chr" $INFILE | awk -v GSIZE=$ATGC_refSize '{SUM += $11 * $3} END{ printf "%0.2f", SUM/GSIZE }')
track_item depth_50x $P50

# average coverage: chromosome read count * read size summed, divided by number of non-N nucleotides
#AVGCOV=$(grep "^chr" $INFILE | awk -v RSIZE=$RSIZE '{SUM += $4 * RSIZE / $3} END{ printf "%0.2f", SUM/NR }')
AVGCOV=$(grep "^chr" $INFILE | awk -v RSIZE=$RSIZE -v GSIZE=$ATGC_refSize  '{SUM += $4 * RSIZE} END{ printf "%0.2f", SUM/GSIZE }')

track_item depth_avg $AVGCOV
export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
echo "RESULTSPATH: " $RESULTSPATH

if [ ! -z $RESULTSPATH ];then
    echo "uploading depth of coverage file"
     F=$(basename $INFILE)
     aws s3 cp "$INFILE" "$RESULTSPATH/$F"

     MEM=$(free -b | grep Mem | awk '{printf "%0.f", $2 * 1.88}')
     qconf -rattr exechost complex_values h_vmem=$MEM `hostname -f`
else
   echoerr "Not uploading, no \$RESULTSPATH"
   track_item current_operation "DepthOfCoverage-upload-ErrorState"
fi
