#!/bin/bash
#####
# Created: April-19-2018
# Stage1 Count reads with mapping quality >=30
#         Sambamba
#####
# mq30.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib
# @param (-m TMP)       - tmp

while getopts dn:i:p:b:m: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      b) LIB="$OPTARG";;
      m) TMP="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
source ${LIB}/seqfile_functions.sh


track_item current_operation "MQ-Count"

CT=$($SAMBAMBA view -c -F ' mapping_quality>=30 ' ${INPUTBAM})

track_item mq_read_ct $CT

