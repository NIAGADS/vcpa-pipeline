#!/usr/bin/perl -w
# Created: Jun-21-2015
# Usage: | split-sam-stream.pl OUTPUT.PREFIX
#

use strict;
use warnings;

my @line;
my $chr;
my %fhHash;

exit unless $ARGV[0];
my $prefix = $ARGV[0];

my $haveHeader = 0;
my $samHeader;

my @chrArray = qw(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y M);
my $zipper = "pigz -R9";

# open file handles for all chrs
for my $i (@chrArray){
  open($fhHash{$i}{'FH'}, "| $zipper > $prefix.chr$i.sam.gz") or die "$!\n";
}

open($fhHash{'Other'}{'FH'}, "| $zipper > $prefix.other.sam.gz") or die "$!\n";
open($fhHash{'Unknown'}{'FH'}, "| $zipper > $prefix.unknown.sam.gz") or die "$!\n";

#
while (<STDIN>) {
 #  HS2000-355_218:4:1101:2171:12131        147     chr17   38776865        60      100M    =       38776734        -231    AC
        @line = split;

        # save/print SAM header
        if (!$haveHeader){

          if ($line[0] =~ /^@/){
            $samHeader .= $_ ;
            next;
          }elsif ($line[0] !~ /^@/){ # no more header lines
            $haveHeader = 1;

            # write header to all chr-sam files
            for my $i (@chrArray){
               print { $fhHash{$i}{'FH'} } $samHeader;
            }

            print { $fhHash{'Other'}{'FH'} } $samHeader;
            print { $fhHash{'Unknown'}{'FH'} } $samHeader;
          }
        }

        if ($line[2] eq "*"){
           print { $fhHash{'Unknown'}{'FH'} } join("\t",@line) , "\n";
           next;
        }

        $chr = $line[2];
        $chr =~ s/^chr//;

        if (exists $fhHash{$chr}){
          print { $fhHash{$chr}{'FH'} } join("\t",@line) , "\n";
        }else{
          print { $fhHash{'Other'}{'FH'} } join("\t",@line) , "\n";
        }


}

# close file handles
for my $i (@chrArray){
  close($fhHash{$i}{'FH'});
}

close($fhHash{'Other'}{'FH'});
close($fhHash{'Unknown'}{'FH'});
