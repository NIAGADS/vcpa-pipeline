#!/bin/bash
#####
# Created: July-31-2017
# Stage0 Roll back and Map reads to reference genome
#         Picard RevertSam
#         Picard MarkIlluminaAdapters
#####

# streamBAM2bwa.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-t THREADS)   - number of thread
# @param (-r RG)        - read-group for filtering by
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib
# @param (-m TMP)       - tmp


while getopts dnt:i:p:r:b:m: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
      m) TMP="$OPTARG";;
   esac
done

set -x
source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
NOW=$(date '+%F+%T')
SRT=$(date +%s)
track_item streamBAM2bwa_start_tm $NOW
track_item current_operation "SamToFastq-BWA"

track_item instance_ami $(get_instance_ami)
track_item stage0_instance_type $(get_instance_type)
track_item instance_git_commit $(git -C $DIR rev-parse --short HEAD)

  RG_LINE=$($SAMTOOLS view -H ${INPUTBAM} |grep  "^@RG" | grep -m 1 -P "ID:${RG}\s" |sed 's/\t/\\t/g')
  echo $RG_LINE
  java -Xmx500M -jar $PICARD  RevertSam \
            TMP_DIR=${TMP} \
            OUTPUT_BY_READGROUP=false \
            RESTORE_ORIGINAL_QUALITIES=true \
            SORT_ORDER=queryname \
            COMPRESSION_LEVEL=0 \
            VALIDATION_STRINGENCY=SILENT \
            QUIET=true \
            INPUT=${INPUTBAM} \
            OUTPUT=/dev/stdout | \
               java -Xmx500M -jar $PICARD  MarkIlluminaAdapters \
                   TMP_DIR=${TMP} \
                   METRICS=${PREFIX}/${RG}.metrics \
                   COMPRESSION_LEVEL=0 \
                   VALIDATION_STRINGENCY=SILENT \
                   QUIET=true \
                   INPUT=/dev/stdin \
                   OUTPUT=/dev/stdout | \
                      java -Xmx2G -jar $PICARD SamToFastq \
                          CLIPPING_ATTRIBUTE=XT \
                          CLIPPING_ACTION=2 \
                          INTERLEAVE=true \
                          INCLUDE_NON_PF_READS=true \
                          INCLUDE_NON_PRIMARY_ALIGNMENTS=false \
                          TMP_DIR=${TMP} \
                          COMPRESSION_LEVEL=0 \
                          VALIDATION_STRINGENCY=SILENT \
                          QUIET=true \
                          INPUT=/dev/stdin \
                          FASTQ=/dev/stdout | \
                             $BWA mem \
                                 -t "${THREADS}" \
                                 -K 100000000 \
                                 -Y \
                                 -R "$RG_LINE" \
                                 -p $REF_FASTA - | \
                                    ${LIB}/split-sam-stream.pl ${PREFIX}/${RG}.aligned

   rm -rf $LOGS/${RG}.sambambarsort.log

NOW=$(date '+%F+%T')
END=$(date +%s)
TM=$[$END-$SRT]

track_item streamBAM2bwa_done_tm $NOW
track_item streamBAM2bwa_duration $TM
