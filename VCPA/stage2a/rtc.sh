#!/bin/bash
#####
# Created: May-01-2017
# Stage2a Recalibrate reads on BAMs
#         GATK RealignTargetCreator
#####

# rtc.sh
#
# @param (-i INPUTBAM)  - input BAM
# @param (-r RG)        - read-group for filtering by
# @param (-p PREFIX)    - out-file naming prefix
# @param (-b LIB)       - lib
# @param (-m TMP)       - tmp
# @param (-t THREADS)   - threads

while getopts dn:i:p:r:b:m:t: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      r) RG="$OPTARG";;
      b) LIB="$OPTARG";;
      m) TMP="$OPTARG";;
      t) THREADS="$OPTARG";;
   esac
done


source ${LIB}/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source ${LIB}/common_functions.sh
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)
SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item rtc_start_tm $NOW
track_item current_operation "RealignerTargetCreator-$THREADS"

    java -Djava.io.tmpdir=${TMP} \
         -jar $GATK \
         -T RealignerTargetCreator\
         -R $REF_FASTA \
         -I ${INPUTBAM} \
         -nt ${THREADS}  \
         --known $KNOWN \
         --known $DBSNP \
         --known $GOLD \
         -o ${PREFIX}.indels.intervals

if [ ! -s "${PREFIX}.indels.intervals" ];then
  error-to-hannah "RealignerTargetCreator-ErrorState on $IID;$SM"
  track_item current_operation "RealignerTargetCreator-ErrorState"
else
  END=$(date +%s)
  TM=$[$END-$SRT];
  NOW=$(date '+%F+%T')

  track_item rtc_duration $TM
  track_item rtc_done_tm $NOW

fi

if [ -z "$RESULTSPATH" ];then
   export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
fi

if [ ! -z "$RESULTSPATH" ];then
   TBL=$(basename ${PREFIX}.indels.intervals)

   gzip ${PREFIX}.indels.intervals

   aws s3 cp --storage-class STANDARD_IA "${PREFIX}.indels.intervals.gz" $RESULTSPATH/$TBL.gz
   S3TPATH=$(rawurlencode "$RESULTSPATH/$TBL.gz")
   track_item rtc_s3_path $S3TPATH

   gunzip ${PREFIX}.indels.intervals.gz
fi
