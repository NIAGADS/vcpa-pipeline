#!/bin/bash


while getopts di: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      i) SAMPLE="$OPTARG";;
   esac
done


 RGRPS=$(samtools view -H  $SAMPLE | grep "^@RG" | grep -Po "ID:\S+" | sed 's/ID://' | sort | uniq)
 echo "${RGRPS[@]}"
